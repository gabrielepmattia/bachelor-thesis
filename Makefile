# @Author: Gabriele Proietti Mattia <gabry3795>
# @Date:   Jan 07, 2017
# @Email:  gabry.gabry@hotmail.it
# @Filename: Makefile
# @Last modified by:   gabry3795
# @Last modified time: Feb 05, 2017


CC := gcc
SRCDIR := src
BUILDDIR := build
TARGET_DIR := bin
TARGET_CLIENT := bin/client
TARGET_SERVER := bin/server

SRCEXT := c
#SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
#SOURCES_CLIENT := $(shell find $(SRCDIR)/client -type f -name *.$(SRCEXT))
#SOURCES_SERVER := $(shell find $(SRCDIR)/server -type f -name *.$(SRCEXT))
#OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
#OBJECTS_CLIENT := $(patsubst $(SRCDIR)/client/%,$(BUILDDIR)/client/%,$(SOURCES_CLIENT:.$(SRCEXT)=.o))
#OBJECTS_SERVER := $(patsubst $(SRCDIR)/server/%,$(BUILDDIR)/server/%,$(SOURCES_SERVER:.$(SRCEXT)=.o))
CFLAGS := -std=gnu99 -Wall -DCOLOR_LOG # -g
LIBS := -lpthread
INC := -Iinclude

default: $(TARGET_SERVER) $(TARGET_CLIENT)

clean:
	@echo "==> Cleaning"
	@echo " $(RM) -r $(BUILDDIR) $(TARGET_DIR)"; $(RM) -r $(BUILDDIR) $(TARGET_DIR)

bin/test: $(BUILDDIR)/log.o $(BUILDDIR)/common.o $(BUILDDIR)/test.o
	@echo "==> [TEST] Linking for executable..."
	@mkdir -p $(TARGET_DIR)
	@echo -e "\t$(CC) $^ -o $@ $(LIBS)"; $(CC) $^ -o $@ $(LIBS)

bin/testui: $(BUILDDIR)/log.o $(BUILDDIR)/common.o $(BUILDDIR)/testui.o $(BUILDDIR)/ui.o
	@echo "==> [TEST] Linking for executable..."
	@mkdir -p $(TARGET_DIR)
	@echo -e "\t$(CC) $^ -o $@ $(LIBS)"; $(CC) $^ -o $@ $(LIBS)

$(BUILDDIR)/test.o : test/test.c
	@echo "==> [TEST] Linking for executable..."
	@mkdir -p $(BUILDDIR)
	@echo -e "\t$(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(BUILDDIR)/testui.o : test/testui.c
	@echo "==> [TEST] Linking for executable..."
	@mkdir -p $(BUILDDIR)
	@echo -e "\t$(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

.PHONY: clean

# Target rules
## Client sources watching

#$(TARGET_CLIENT): $(OBJECTS_CLIENT)
#	@echo "==> [CLIENT] Linking for executable..."
#	@mkdir -p $(TARGET_DIR)
#	@echo -e "\t$(CC) $^ -o $(TARGET_CLIENT) $(LIB)"; $(CC) $^ -o $(TARGET_CLIENT) $(LIB)

#$(BUILDDIR)/client/%.o: $(SRCDIR)/client/%.$(SRCEXT)
#	@echo "==> [CLIENT] Compiling files..."
#	@mkdir -p $(BUILDDIR)/client
#	@echo -e "\t$(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

## Server sources watching
#$(TARGET_SERVER): $(OBJECTS_SERVER)
#	@echo "==> [SERVER] Linking for executable..."
#	@mkdir -p $(TARGET_DIR)
#	@echo -e "\t$(CC) $^ -o $(TARGET_SERVER) $(LIB)"; $(CC) $^ -o $(TARGET_SERVER) $(LIB)

#$(BUILDDIR)/server/%.o: $(SRCDIR)/server/%.$(SRCEXT)
#	@echo "==> [SERVER] Compiling files..."
#	@mkdir -p $(BUILDDIR)/server
#	@echo -e "\t$(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<

$(TARGET_CLIENT): $(BUILDDIR)/client.o $(BUILDDIR)/log.o $(BUILDDIR)/common.o $(BUILDDIR)/ui.o $(BUILDDIR)/client_ui.o
	@echo "==> [CLIENT] Linking for executable..."
	@mkdir -p $(TARGET_DIR)
	@echo -e "\t$(CC) $^ -o $(TARGET_CLIENT) $(LIBS)"; $(CC) $^ -o $(TARGET_CLIENT) $(LIBS)

$(TARGET_SERVER): $(BUILDDIR)/server.o $(BUILDDIR)/log.o $(BUILDDIR)/common.o
	@echo "==> [SERVER] Linking for executable..."
	@mkdir -p $(TARGET_DIR)
	@echo -e "\t$(CC) $^ -o $(TARGET_SERVER) $(LIBS)"; $(CC) $^ -o $(TARGET_SERVER) $(LIBS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@echo "==> Compiling objects..."
	@mkdir -p $(BUILDDIR)
	@echo -e "\t$(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<
