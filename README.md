<!--
@Author: Gabriele Proietti Mattia <gabry3795>
@Date:   Jan 18, 2017
@Email:  gabry.gabry@hotmail.it
@Filename: README.md
@Last modified by:   gabry3795
@Last modified time: Jan 30, 2017
-->

# Bachelor Thesis

_Gabriele Proietti Mattia - Matricola 1645351_

[![pipeline status](https://gitlab.com/gabrielepmattia/bachelor-thesis/badges/master/pipeline.svg)](https://gitlab.com/gabrielepmattia/bachelor-thesis/commits/master)

- **Title**: Servizio di messaggistica da terminale con interfaccia utente reattiva
- **Title [EN]**: Terminal instant messaging service with responsive UI
- **Language**: Italian
- **Major**: Master of Science in Engineering in Computer Science
- **University**: Sapienza University of Rome
- **A.Y.**: 2016 - 2017

---

This is is my bachelor thesis for the Computer Engineering major. The following builds are available:

- **Thesis** [[PDF](https://gitlab.com/gabrielepmattia/bachelor-thesis/-/jobs/artifacts/master/raw/doc/proj-thesis.pdf?job=build-doc)] that is released with the CC-BY-SA-4.0 license

Sources are released with GPLv3 license.

---

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

Every work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
