/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  ui.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 27, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <common.h>
#include <stdarg.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>

#include <ui.h>

// Buffers
char **video_buffer;                   // Entire received buffer to be printed on the screen
char **screen_buffer;                  // Single terminal page buffer
char input_buffer[BUFFER_SIZE] = "\0"; // Input buffer

// Semaphores
sem_t input_ready;
sem_t input_taken;

// Buffer sizes
unsigned int video_buffer_lines = 0;
unsigned int lines; // Screen buffer lines
unsigned int cols;  // Screen buffer cols
size_t input_buffer_size;

unsigned int current_line = 0; // Current scrolled line
unsigned int input_offset = 0; // Offset if cmdline exceed one line

unsigned int requested_closing = 0; // 1 if application is about to terminate

pthread_t ui_core_tid; // Core key handler thread

/**
 * Prepare ui buffers, always call this method before creating windows!
 * @return result code
 */
int init_ui()
{
  // Get terminal size
  struct winsize w;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

  lines = w.ws_row;
  cols = w.ws_col;
  if (!lines || !cols)
    return -1;

  video_buffer = NULL;
  screen_buffer = (char **)malloc(lines * sizeof(char *));
  for (int i = 0; i < lines; i++)
  {
    screen_buffer[i] = (char *)malloc(cols * sizeof(char));
    bzero(screen_buffer[i], cols);
  }

  sem_init(&input_ready, 0, 0);
  sem_init(&input_taken, 0, 0);
  set_input_mode(~(ICANON | ECHO), 1); // Set terminal non canonical

  pthread_create(&ui_core_tid, NULL, ui_core, NULL);
  //pthread_detach(ui_core_tid);

  return 0;
}

/**
 * Safely close the ui
 * @return result
 */
void close_ui()
{
  requested_closing = 1;
  sem_post(&input_ready);
  pthread_cancel(ui_core_tid);
  pthread_join(ui_core_tid, NULL);
  flushs();
  flushs_screen();
  sem_close(&input_ready);
  sem_close(&input_taken);
}

/**
 * This function add a string to video buffer
 * @param  format  format of string
 * @param  VARARGS arguments
 * @return         characters written
 */
int prints(char *format, ...)
{
  va_list arg;
  int done;
  char buf[MAX_LINE_BUFFER_SIZE];

  va_start(arg, format);
  done = vsnprintf(buf, MAX_LINE_BUFFER_SIZE, format, arg);
  va_end(arg);

  if (done < 0)
    return -1;

  if (video_buffer == NULL)
    video_buffer = (char **)malloc(++video_buffer_lines * sizeof(char *));
  else
    video_buffer = realloc(video_buffer, (++video_buffer_lines) * sizeof(char *));

  size_t buf_len = strlen(buf);
  video_buffer[video_buffer_lines - 1] = (char *)malloc(buf_len * sizeof(char) + 1);
  snprintf(video_buffer[video_buffer_lines - 1], buf_len + 1, "%s", buf);

  return done;
}

/**
 * Prints a line with a char specified
 * @param c [description]
 */
void printls(char *c)
{
  char line[MAX_LINE_BUFFER_SIZE] = "";
  for (int i = 0; i < cols - 1; i++)
    strncat(line, c, 1);
  prints(line);
}

/**
 * Reload the screen_buffer
 * @return [description]
 */
int refreshs()
{
  ttyclear();
  int from_line = current_line;
  int line_size, i = 0, offset;

  while (i < lines - 2 - input_offset)
  {
    if (from_line >= video_buffer_lines)
    {
      printf("\n");
      i++;
      continue;
    }

    line_size = strlen(video_buffer[from_line]);

    if (line_size == 0)
    {
      printf("\n");
      from_line++;
      i++;
    }
    else
    {

      offset = 0;
      do
      {

        snprintf(screen_buffer[i], cols, "%s", video_buffer[from_line] + offset);
        printf("%s\n", screen_buffer[i]);

        offset += (cols - 1);
        i++;

      } while (offset < line_size && i < lines - 2 - input_offset);
      from_line++;
    }
  }
  for (int i = 0; i < cols; i++)
    printf("\u2500");                          // Print dashes
  printf(COMMAND_LINE_CHR "%s", input_buffer); // User command line

  fflush(stdout);
  return 0;
}

/**
 * Update the current line and refresh
 * @param d direction
 */
void scroll(unsigned char d)
{
  if (d == 'U')
  {
    if (current_line == 0)
      return;
    current_line--;
    refreshs();
  }
  else
  {
    if (current_line == video_buffer_lines - cols)
      return;
    current_line++;
    refreshs();
  }
}

/**
 * Totally free the video_buffer
 * @return [description]
 */
int flushs()
{
  for (int i = 0; i < video_buffer_lines; i++)
  {
    free(video_buffer[i]);
  }
  free(video_buffer);
  video_buffer = NULL;
  video_buffer_lines = 0;
  return 0;
}

/**
 * This function flushes the screen buffer at exit
 * @return result_code
 */
int flushs_screen()
{
  for (int i = 0; i < lines; i++)
  {
    free(screen_buffer[i]);
  }
  free(video_buffer);
  return 0;
}

/**
 * Blocking method that return when input is available
 * @param input pointer where write the buffer
 * @return read bytes
 */
int get_input(char *input)
{
  int read_bytes_bak;

  sem_wait(&input_ready);
  if (requested_closing)
    return -1;

  snprintf(input, input_buffer_size + 1, "%s\n", input_buffer);
  bzero(&input_buffer, BUFFER_SIZE);
  read_bytes_bak = input_buffer_size;
  input_buffer[0] = '\0';
  input_buffer_size = 0;
  input_offset = 0;

  sem_post(&input_taken);

  return read_bytes_bak;
}

/**
 * Core thread that handles keys
 */
void *ui_core()
{
  char c, c1, c2;
  int current_cmdline_i = 0;
  char input_buffer_bk[BUFFER_SIZE];

  while (1)
  { // Parse line
    current_cmdline_i = 0;
    while (1)
    { // Parse character
      scanf("%c", &c);

      if (c == '\n')
        break;

      // Backslash
      else if (c == 127 || c == 7)
      {
        if (current_cmdline_i == 0 || input_buffer_size == 0)
          continue;

        if (current_cmdline_i < input_buffer_size)
        {
          memcpy(input_buffer_bk, input_buffer, BUFFER_SIZE);
          memcpy(input_buffer, input_buffer_bk, current_cmdline_i - 1);
          memcpy(input_buffer + current_cmdline_i - 1,
                 input_buffer_bk + current_cmdline_i,
                 input_buffer_size - current_cmdline_i);

          input_buffer_size--;
          current_cmdline_i--;
          input_buffer[input_buffer_size] = '\0';
          refreshs();
          for (int i = 0; i < input_buffer_size - current_cmdline_i; i++)
            move_cursor_left();
        }
        else
        {
          input_buffer[input_buffer_size - 1] = '\0';
          input_buffer_size--;
          current_cmdline_i--;
          //move_cursor_left(); // move cursor LEFT
          refreshs();
        }
      }

      // Keys
      else if (c == 27)
      {
        scanf("%c", &c1);
        scanf("%c", &c2);
        if (c1 == 91)
        {
          if (c2 == 65)
            scroll('U'); // move page
          if (c2 == 66)
            scroll('D'); // move page
          if (c2 == 67)
          { // move cursor RIGHT
            if (input_buffer_size == 0 || current_cmdline_i == input_buffer_size)
              continue;
            // Check if we reach the end of the line form left
            if ((current_cmdline_i + 3) % cols == 0)
            {
              move_cursor_down();
              for (int i = 0; i < cols; i++)
                move_cursor_left();
            }

            current_cmdline_i++;
            move_cursor_right();
          }
          if (c2 == 68)
          { // move cursor LEFT
            if (current_cmdline_i == 0 || input_buffer_size == 0)
              continue;
            // Check if we reach the end of the line form left
            if ((current_cmdline_i + 3) % cols == 0)
            {
              move_cursor_up();
              for (int i = 0; i < cols; i++)
                move_cursor_right();
            }
            current_cmdline_i--;
            move_cursor_left();
          }
        }
      } // key character

      else
      {

        // Implement insert character inside cmdline
        if (current_cmdline_i < input_buffer_size)
        {
          memcpy(input_buffer_bk, input_buffer, BUFFER_SIZE);
          memcpy(input_buffer, input_buffer_bk, current_cmdline_i);
          input_buffer[current_cmdline_i] = c;
          current_cmdline_i++;
          memcpy(input_buffer + current_cmdline_i,
                 input_buffer_bk + current_cmdline_i - 1,
                 input_buffer_size - current_cmdline_i + 1);

          input_buffer_size++;
          input_buffer[input_buffer_size + 1] = '\0';
          refreshs();

          for (int i = 0; i < input_buffer_size - current_cmdline_i; i++)
            move_cursor_left();
        }
        else
        {

          // Normal character
          input_buffer[input_buffer_size] = c;
          current_cmdline_i++;
          input_buffer_size++;
          input_buffer[input_buffer_size + 1] = '\0';
          refreshs();
        }

        // Check if input requires more than one line, if that set input offset used in refreshs
        if ((int)(input_buffer_size / (cols - COMMAND_LINE_CHR_N)) > 0)
        {
          input_offset = (int)(input_buffer_size / (cols - COMMAND_LINE_CHR_N));
          refreshs();
        }

      } // normal character input
    }   // internal while

    sem_post(&input_ready);
    sem_wait(&input_taken);
  } // outer while
}

// Termios
// https://www.gnu.org/software/libc/manual/html_node/Noncanon-Example.html
struct termios saved_attributes;

/**
 * Restore the previous terminal input mode
 */
void reset_input_mode()
{
  tcsetattr(STDIN_FILENO, TCSANOW, &saved_attributes);
}

/**
 * Set the input mode
 * @param flags           modes flags
 * @param restore_at_exit set if previous terminal mode has to be restored when
 *                        application exits
 */
void set_input_mode(tcflag_t flags, int restore_at_exit)
{
  struct termios tattr;
  //char *name;

  /* Make sure stdin is a terminal. */
  if (!isatty(STDIN_FILENO))
  {
    fprintf(stderr, "Not a terminal.\n");
    exit(EXIT_FAILURE);
  }

  /* Save the terminal attributes so we can restore them later. */
  tcgetattr(STDIN_FILENO, &saved_attributes);
  if (restore_at_exit)
    atexit(reset_input_mode);

  /* Set the funny terminal modes. */
  tcgetattr(STDIN_FILENO, &tattr);
  tattr.c_lflag &= flags; // ~(ICANON|ECHO); /* Clear ICANON and ECHO. */
  tattr.c_cc[VMIN] = 1;
  tattr.c_cc[VTIME] = 0;
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &tattr);
}
