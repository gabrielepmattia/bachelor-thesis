/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  common.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 28, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <common.h>
#include <log.h>

/**
 * Parse a server response from a server response string, return -1 if response
 * is damaged.
 *
 * Server response is <result_code>;<message>
 *
 * Note: We use here thread safe strtok_r because this method can be called by multiple
 * server threads
 *
 * @param response
 * @return
 */
int parse_server_response(char *response_s_in, server_response_t *response_t_out)
{
  char *context, *current_token;

  // First call to strtok grabs server response code
  current_token = strtok_r(response_s_in, COMMAND_DELIMITER, &context);
  if (current_token == NULL)
    return -1;
  response_t_out->result_code = atoi(current_token);

  // Second call grab parameters numbers
  current_token = strtok_r(NULL, COMMAND_DELIMITER, &context);
  if (current_token == NULL)
    return -1;
  response_t_out->args_n = atoi(current_token);

  // Third and next calls grabs params
  response_t_out->args = (char **)malloc(response_t_out->args_n * sizeof(char *));
  for (int i = 0; i < response_t_out->args_n; i++)
  {
    current_token = strtok_r(NULL, COMMAND_DELIMITER, &context);
    response_t_out->args[i] = (char *)malloc((strlen(current_token) + 1) * sizeof(char));
    snprintf(response_t_out->args[i], (strlen(current_token) + 1), current_token);
  }

  return 0;
}

/**
 * Translate a response structure to string to send to server
 * @param response
 * @return
 */
int prepare_string_from_response(server_response_t *response_t_in, char *response_s_out)
{
  //snprintf(response_s_out, STR_BUFFER_SIZE, "%d;%s", response_t_in->result_code, response_t_in->message);

  if (response_t_in->args_n == 0)
  {
    loge("[prepare_string_from_response] Comman has args_n == 0");
    return -1;
  }

  snprintf(response_s_out, STR_BUFFER_SIZE, "%d;%d;", response_t_in->result_code, response_t_in->args_n);

  // Add the commands
  for (int i = 0; i < response_t_in->args_n; i++)
  {
    strncat(response_s_out, response_t_in->args[i], strlen(response_t_in->args[i]));
    if (i != response_t_in->args_n - 1)
      strncat(response_s_out, ";", 1);
  }
  strncat(response_s_out, "\0", 1);

  logd("[prepare_string_from_response] prepared response str is %s", response_s_out);

  return 0;
}

/**
 * Parse a server command from a command string
 *
 * A command is <code>;<number of parameters>;<parameters>
 *
 * We use here thread safe strtok_r because this method can be called by multiple
 * server threads
 *
 * @param command
 * @return
 */
int parse_server_command(char *command_s_in, server_command_t *command_t_out)
{
  char *context = "", *current_token;

  // First call to strtok grabs command ID
  current_token = strtok_r(command_s_in, COMMAND_DELIMITER, &context);
  if (current_token == NULL)
    return -1;
  command_t_out->cmd = current_token[0];

  // Second call grab parameters numbers
  current_token = strtok_r(NULL, COMMAND_DELIMITER, &context);
  if (current_token == NULL)
    return -1;
  command_t_out->args_n = atoi(current_token);
  // Third and next calls grabs params
  int i;
  for (i = 0; i < command_t_out->args_n; i++)
  {
    current_token = strtok_r(NULL, COMMAND_DELIMITER, &context);
    // Check if arguments are less than expected
    if (i < command_t_out->args_n && current_token == NULL)
      return -1;

    strncpy(command_t_out->args[i], current_token, strlen(current_token) + 1);
  }
  return 0;
}

// TODO Prepare command

/**
 * Prepare a string to send to server from a command. We assume that commands
 * have 10 args max
 * @param command
 * @return
 */
int prepare_string_from_command(server_command_t *command_t_in, char *command_s_out)
{
  if (command_t_in->args_n > COMMAND_MAX_ARGS)
    return -1;
  snprintf(command_s_out, STR_BUFFER_SIZE, "%c;%d;", command_t_in->cmd, command_t_in->args_n);
  // Add the commands
  int i;
  for (i = 0; i < command_t_in->args_n; i++)
  {
    strncat(command_s_out, command_t_in->args[i], strlen(command_t_in->args[i]));
    if (i != command_t_in->args_n - 1)
      strncat(command_s_out, ";", 1);
  }
  strncat(command_s_out, "\0", 1);
  return 0;
}

/**
 * Free server response memory
 * @param response_t_in pointer to response struct
 */
void free_server_response(server_response_t *response_t_in)
{
  for (int i = 0; i < response_t_in->args_n; i++)
  {
    free(response_t_in->args[i]);
  }
  free(response_t_in->args);
}

/**
 * This function parses a generic COMMAND_DELIMITER separated string
 * @param  s_in input string
 * @param  out  point to a char**
 * @return      number of tokes parsed, elements of char**
 */
int generic_parser(char *s_in, char ***out)
{
  char *context = "", *current_token;
  int n = 0;

  while (1)
  {
    current_token = strtok_r(n == 0 ? s_in : NULL, COMMAND_DELIMITER, &context);
    if (current_token == NULL)
      break;

    if (n == 0)
      *out = (char **)malloc(++n * sizeof(char *));
    else
      *out = (char **)realloc(*out, ++n * sizeof(char *));

    (*out)[n - 1] = (char *)malloc((strlen(current_token) + 1) * sizeof(char));
    snprintf((*out)[n - 1], (strlen(current_token) + 1), "%s", current_token);
  }
  return n;
}

void free_matrix(char **m, int n)
{
  for (int i = 0; i < n; i++)
  {
    free(m[i]);
  }
  free(m);
}
