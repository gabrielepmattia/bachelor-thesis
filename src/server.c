/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  server.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 28, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>  // htons()
#include <netinet/in.h> // struct sockaddr_in
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>

#include <server.h>
#include <log.h>

// Store here, in memory, the users data
user_t **db;
int db_length = 0;
sem_t write_db_sem;

/**
 * This is the starting point of the server application
 *
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv)
{
  register_signals();

  sem_init(&write_db_sem, 0, 1); // binary sem for writing db
  ttyclear();
  printf("This is a Messaging Application - Server.\n");
  printf("Written by Gabriele Proietti Mattia - Facoltà di Ingegneria Informatica\n");
  printf("Operating Systems Project - Teacher: Francesco Quaglia\n\n");
  int server_socket_fd, client_socket_fd, ret;

  while ((ret = mkdir(SERVER_FILES_PATH, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0))
  {
    if (errno == EEXIST)
      break;
    logfe("Can't create server files " SERVER_FILES_PATH " dir");
  }

  // Load the current database
  ret = load_database();
  if (ret < 0)
    logfe("Can't read database");

  logd("Database loaded successfully [%d entries]", db_length);
  if (DB_DEBUG)
    print_db();

  struct sockaddr_in server_address = {0};
  server_address.sin_addr.s_addr = INADDR_ANY;
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(SERVER_PORT);
  int sockaddr_len = sizeof(struct sockaddr_in);

  server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (server_socket_fd < 0)
    logfe("There was an error creating the socket");

  int reuseaddr_opt = 1;
  ret = setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt, sizeof(reuseaddr_opt));
  if (ret < 0)
    loge("Can't set opt");

  ret = bind(server_socket_fd, (struct sockaddr *)&server_address, sockaddr_len);
  if (ret < 0)
    logfe("Can't bind socket");

  ret = listen(server_socket_fd, MAX_SERVER_CONNECTIONS);
  if (ret < 0)
    logfe("Can't listen to socket");

  // Prepare the struct that will be filled with client that will connect to this server
  struct sockaddr_in *client_address = calloc(1, sizeof(struct sockaddr_in));
  if (ret < 0)
    logs("Server has started. Listening...");

  while (1)
  {
    client_socket_fd = accept(server_socket_fd, (struct sockaddr *)client_address, (socklen_t *)&sockaddr_len);
    if (client_socket_fd == -1)
    {
      if (errno == EINTR)
        continue;
      loge("Can't accept connection");
    }

    logs("Connection accepted with client %s:%u",
         inet_ntoa(client_address->sin_addr),
         ntohs(client_address->sin_port));

    // Prepare the arguments to pass to the thread
    connection_handler_args_t *args = (connection_handler_args_t *)malloc(sizeof(connection_handler_args_t));
    args->address = client_address;
    args->socket_fd = client_socket_fd;
    pthread_t thread;

    // Start thread handler here
    if (pthread_create(&thread, NULL, client_connection_handler, (void *)args) != 0)
      if (ret < 0)
        loge("Can't create a new thread, error %d", errno);

    pthread_detach(thread);

    client_address = calloc(1, sizeof(struct sockaddr_in));
  }

  exit(EXIT_SUCCESS);
}

/**
 * Main thread handler
 *
 * @param args Client arguments to handle
 * @return
 */
void *client_connection_handler(void *args)
{
  int ret, recv_bytes, already_registered, close_connection, logged, user_id;
  char line_buffer[STR_BUFFER_SIZE];
  connection_handler_args_t *client_args = (connection_handler_args_t *)args;
  server_command_t received_command;
  server_response_t server_response;

  // Resolve client IP
  char client_ip[INET_ADDRSTRLEN];
  inet_ntop(AF_INET, &(client_args->address->sin_addr), client_ip, INET_ADDRSTRLEN);
  uint16_t client_port = ntohs(client_args->address->sin_port);

  char buffer[BUFFER_SIZE];
  size_t buffer_length = sizeof(buffer);

  logged = 0;

  // Handler core
  while (1)
  {
    bzero(&buffer, buffer_length);
    bzero(received_command.args, COMMAND_MAX_ARGS * STR_BUFFER_SIZE);
    bzero(&received_command, sizeof(server_command_t));

    already_registered = 0;
    close_connection = 0;
    recv_bytes = 0;

    // Receive bytes from the client
    while (1)
    {
      ret = recv(client_args->socket_fd, buffer + recv_bytes, 1, 0);
      if (ret < 0)
      {
        if (errno == EINTR)
          continue;
        loge("Recv failed. Thread handler is dead for %s", client_ip);
        pthread_exit(NULL);
      }
      if (ret == 0)
      {
        loge("Peer improperly closed the connection. Thread handler is dead for %s", client_ip);
        pthread_exit(NULL);
      }

      recv_bytes += ret;
      if (buffer[recv_bytes - 1] == '\0')
        break;
    }

    // Check received command
    ret = parse_server_command(buffer, &received_command);
    if (ret < 0)
    {
      loge("Error parsing received command string");
      ret = make_response_single_arg(client_args->socket_fd, SERVER_ERROR_CODE,
                                     "Command is malformed, check that all fields are filled");
      close_connection = 1;
      break;
    }

    switch (received_command.cmd)
    {
    case REGISTER_SERVER_CMD:
      logi("Client %s:%u requested registration", client_ip, client_port);
      int i;
      for (i = 0; i < db_length; i++)
      {
        if (strcmp(received_command.args[0], db[i]->nickname) == 0)
        {

          ret = make_response_single_arg(client_args->socket_fd, SERVER_ERROR_CODE,
                                         "User already registered");

          already_registered = 1;
          close_connection = 1;
          break;
        }
      }
      if (!already_registered)
      {
        // Handle true registration
        user_t *user = (user_t *)malloc(sizeof(user_t));
        snprintf(user->nickname, strlen(received_command.args[0]) + 1, "%s", received_command.args[0]);
        snprintf(user->password, strlen(received_command.args[1]) + 1, "%s", received_command.args[1]);
        ret = add_user_to_database(user);

        if (ret < 0)
        {
          loge("Can't add user to database");
          ret = make_response_single_arg(client_args->socket_fd, SERVER_ERROR_CODE,
                                         "There was an error when registering");
        }
        else
        {
          ret = make_response_single_arg(client_args->socket_fd, SERVER_OK_CODE,
                                         "Registered successfully");
          logs("Client %s registered as %s. Now db is %d long", client_ip, user->nickname, db_length);
        }

        if (ret < 0)
          loge("Answer to client failed");
        close_connection = 1;
      }
      break;

    case SEND_MESSAGE_SERVER_CMD: // 5 arguments: user_id, token, recipient, subj, msg
      // Check if user has the right token -- or it's logged
      if (atoi(received_command.args[1]) == db[atoi(received_command.args[0])]->logged_token)
      {
        // Check if recipient exists
        int i = 0;
        for (; i < db_length; i++)
          if (strcmp(received_command.args[2], db[i]->nickname) == 0)
            break;
        if (i >= db_length)
        {
          make_response_single_arg(client_args->socket_fd, SERVER_ERROR_CODE,
                                   "Recipient does not exists");
          close_connection = 1;
          break;
        }

        char user_msgs_file_path[STR_BUFFER_SIZE];
        snprintf(user_msgs_file_path, STR_BUFFER_SIZE, "%s%s",
                 SERVER_FILES_PATH, received_command.args[2]);

        sem_wait(&db[i]->messages_write_sem); // recipient semaphore
        FILE *user_msgs_file = fopen(user_msgs_file_path, "a");
        if (user_msgs_file == NULL)
        {

          make_response_single_arg(client_args->socket_fd, SERVER_ERROR_CODE,
                                   "Can't save message to server");
        }
        else
        { // Message savable

          char buf[STR_BUFFER_SIZE * 2 + MAX_USER_STR_SIZE + 1];
          snprintf(buf, STR_BUFFER_SIZE * 2 + MAX_USER_STR_SIZE + 1, "%d;%d;%s;%s;%s\n",
                   (int)time(NULL),                              // Unix timestamp
                   atoi(received_command.args[0]),               // Sender ID
                   db[atoi(received_command.args[0])]->nickname, // Sender nickname
                   received_command.args[3],                     // Message Subject
                   received_command.args[4]);                    // Message content
          fprintf(user_msgs_file, buf, STR_BUFFER_SIZE * 2 + MAX_USER_STR_SIZE + 1);
          fclose(user_msgs_file);

          make_response_single_arg(
              client_args->socket_fd, SERVER_OK_CODE, "Message sent successfully");

          logs("Message has been sent from %s to %s", received_command.args[0], received_command.args[2]);
        }

        sem_post(&db[i]->messages_write_sem);
      }
      else
      { // User not logged in
        make_response_single_arg(
            client_args->socket_fd, SERVER_ERROR_CODE,
            "You are not logged, please login");
      }

      close_connection = 1;
      break;

    case SYNC_SERVER_CMD:

      logd("User #%s requested messages sync", received_command.args[0]);
      user_id = atoi(received_command.args[0]);

      if (atoi(received_command.args[1]) == db[user_id]->logged_token)
      {
        // Prepare the file path where find messages
        char user_msgs_file_path[STR_BUFFER_SIZE];
        snprintf(user_msgs_file_path, STR_BUFFER_SIZE, "%s%s", SERVER_FILES_PATH,
                 db[atoi(received_command.args[0])]->nickname);

        sem_wait(&db[user_id]->messages_write_sem);
        FILE *user_msgs_file = fopen(user_msgs_file_path, "r");
        logi("Tying to open messages file for user #%s", received_command.args[0]);
        if (user_msgs_file == NULL)
        {
          make_response_single_arg(client_args->socket_fd, SERVER_OK_CODE, "0");
          logi("Messages file does not exists for user %d", user_id);
        }
        else
        { // Scan user file messages
          char ch;
          int lines = 0;
          // Count lines
          while (!feof(user_msgs_file))
          {
            ch = fgetc(user_msgs_file);
            if (ch == '\n')
              lines++;
          }
          rewind(user_msgs_file);

          char lines_s[STR_BUFFER_SIZE];
          snprintf(lines_s, STR_BUFFER_SIZE, "%d", lines);

          // Send to client number of messages for him
          ret = make_response_single_arg(client_args->socket_fd, SERVER_OK_CODE, lines_s);

          while (1)
          {
            // Get the line
            if (fgets(line_buffer, STR_BUFFER_SIZE, user_msgs_file) == NULL)
              break;
            line_buffer[strlen(line_buffer) - 1] = '\0';
            logd("Message line read: %s", line_buffer);

            ret = answer_data_to_client(client_args->socket_fd, line_buffer, strlen(line_buffer) + 1);
            if (ret < 0)
              loge("Can't send line to client");
          }
          fclose(user_msgs_file);
          //fclose(fopen(user_msgs_file_path, "w")); // Truncate file
        }
        sem_post(&db[user_id]->messages_write_sem);
      }
      else
      { // User not logged
        make_response_single_arg(
            client_args->socket_fd, SERVER_ERROR_CODE,
            "You are not logged, please login");
      }
      close_connection = 1;
      break;

    case LOGIN_SERVER_CMD:
      for (i = 0; i < db_length; i++)
      {
        if (strcmp(received_command.args[0], db[i]->nickname) == 0)
          if (strcmp(received_command.args[1], db[i]->password) == 0)
          {
            srand(time(NULL));
            db[i]->logged_time = (unsigned long)time(NULL);
            db[i]->logged_token = rand();

            server_response.result_code = SERVER_OK_CODE;
            server_response.args_n = 2;
            server_response.args = (char **)malloc(server_response.args_n * sizeof(char *));
            server_response.args[0] = (char *)malloc(STR_BUFFER_SIZE * sizeof(char));
            server_response.args[1] = (char *)malloc(STR_BUFFER_SIZE * sizeof(char));
            snprintf(server_response.args[0], STR_BUFFER_SIZE, "%d", db[i]->id);
            snprintf(server_response.args[1], STR_BUFFER_SIZE, "%d", db[i]->logged_token);

            ret = answer_to_client(client_args->socket_fd, &server_response);
            if (ret < 0)
              loge("Answer to client failed");
            free_server_response(&server_response);

            logs("User %s logged successfully with token #%d", db[i]->nickname, db[i]->logged_token);

            logged = 1;
            break;
          }
      }
      if (!logged)
        make_response_single_arg(
            client_args->socket_fd, SERVER_ERROR_CODE, "Username or password are wrong");

      close_connection = 1;
      break;

    case DEL_MESSAGE_SERVER_CMD:

      user_id = atoi(received_command.args[0]);
      int msg_del_n = atoi(received_command.args[2]);
      logd("User #%d requested to delete message #%d", user_id, msg_del_n);
      char **messages_buffer;
      int messages_n = 0, cur_message = 0;

      if (atoi(received_command.args[1]) == db[user_id]->logged_token)
      {

        // Prepare the file path where find messages
        char user_msgs_file_path[STR_BUFFER_SIZE];
        snprintf(user_msgs_file_path, STR_BUFFER_SIZE, "%s%s", SERVER_FILES_PATH,
                 db[atoi(received_command.args[0])]->nickname);

        sem_wait(&db[user_id]->messages_write_sem); // Critical section starts
        FILE *user_msgs_file = fopen(user_msgs_file_path, "r");
        if (user_msgs_file == NULL)
        {
          make_response_single_arg(
              client_args->socket_fd, SERVER_ERROR_CODE, "User has no messages");
          close_connection = 1;
          sem_post(&db[user_id]->messages_write_sem);
          break;
        }

        // Load file
        while (1)
        {
          if (fgets(line_buffer, STR_BUFFER_SIZE, user_msgs_file) == NULL)
            break;
          if (cur_message == msg_del_n)
          {
            cur_message++;
            continue;
          }

          if (messages_n == 0)
            messages_buffer = (char **)malloc(++messages_n * sizeof(char *));
          else
            messages_buffer = (char **)realloc(messages_buffer, ++messages_n * sizeof(char *));

          messages_buffer[messages_n - 1] = (char *)malloc(strlen(line_buffer) + 1);
          snprintf(messages_buffer[messages_n - 1], strlen(line_buffer) + 1, "%s", line_buffer);

          cur_message++;
        }
        fclose(user_msgs_file);

        // Check if messages are 0 in total
        if (messages_n == 0)
        {
          fclose(fopen(user_msgs_file_path, "w"));
          make_response_single_arg(
              client_args->socket_fd, SERVER_OK_CODE, "Inbox emptied");
          sem_post(&db[user_id]->messages_write_sem);
          close_connection = 1;
          break;
        }

        // Message number does not exists
        if (msg_del_n >= cur_message)
        {
          make_response_single_arg(
              client_args->socket_fd, SERVER_ERROR_CODE, "Selected message does not exists");
          sem_post(&db[user_id]->messages_write_sem);
          close_connection = 1;
          break;
        }

        user_msgs_file = fopen(user_msgs_file_path, "w");
        if (user_msgs_file == NULL)
        {
          make_response_single_arg(
              client_args->socket_fd, SERVER_ERROR_CODE, "Can't reopen message file");
          sem_post(&db[user_id]->messages_write_sem);
          close_connection = 1;
          break;
        }

        // Rewrite
        for (int i = 0; i < messages_n; i++)
          fprintf(user_msgs_file, "%s", messages_buffer[i]);
        fclose(user_msgs_file);

        free_matrix(messages_buffer, messages_n);

        close_connection = 1;
        make_response_single_arg(
            client_args->socket_fd, SERVER_OK_CODE, "Message deleted successfully");
      }
      else
      { // User not logged
        make_response_single_arg(
            client_args->socket_fd, SERVER_ERROR_CODE, "You are not logged, please login");
      }
      sem_post(&db[user_id]->messages_write_sem);
      close_connection = 1;
      break;

    default:
      make_response_single_arg(
          client_args->socket_fd, SERVER_ERROR_CODE, "Command given is not valid!");
      break;
    }

    if (close_connection)
    {
      logs("Connection with %s closed", client_ip);
      break;
    }
  }

  ret = close(client_args->socket_fd);
  if (ret < 0)
    loge("Cannot close the socket");

  free(client_args->address);
  free(client_args);
  logd("Connection with client halted gracefully");

  pthread_exit(NULL);
}

/**
 * Answer to client with using a response struct
 *
 * @param socket_fd Socket descriptor
 * @param response Response struct
 * @return -1 if errors, 0 if success
 */
int answer_to_client(int socket_fd, server_response_t *response_t)
{
  char response_s[STR_BUFFER_SIZE * 3];
  if (prepare_string_from_response(response_t, response_s) < 0)
  {
    loge("There was an error during preparing string response");
    return -1;
  }
  return answer_data_to_client(socket_fd, response_s, strlen(response_s) + 1);
}

/**
 * Answer to client with a string
 *
 * @param socket_fd Socket descriptor
 * @param answer Answer string
 * @param answer_length Answer string length
 * @return -1 if errors, 0 if success
 */
int answer_data_to_client(int socket_fd, char *answer, int answer_length)
{
  int ret;
  while ((ret = send(socket_fd, answer, answer_length, 0)) < 0)
  {
    if (errno == EINTR)
      continue;
    if (errno == EPIPE)
      loge("Client improperly closed connection");
    loge("Can't send data to server");
    return -1;
  }
  return 0;
}

/**
 * Create a response with a single argument
 * @param  socket_fd [description]
 * @param  code      [description]
 * @param  message   [description]
 * @return           [description]
 */
int make_response_single_arg(int socket_fd, int code, char *message)
{
  int ret;
  server_response_t server_response;
  server_response.result_code = code;
  server_response.args_n = 1;
  server_response.args = (char **)malloc(sizeof(char *));
  server_response.args[0] = (char *)malloc(STR_BUFFER_SIZE * sizeof(char));
  snprintf(server_response.args[0], STR_BUFFER_SIZE, message);

  ret = answer_to_client(socket_fd, &server_response);
  if (ret < 0)
    loge("Answer to client failed");
  free_server_response(&server_response);

  return ret;
}

/**
 * Parse and load the entire database in memory
 *
 * @return -1 if errors, 0 if success
 */
int load_database()
{
  FILE *database_fd = fopen(SERVER_FILES_PATH DATABASE_FILENAME, "r");
  if (database_fd == NULL)
    return 0;
  char line_buffer[2048];
  char *current_token;
  char *context = "";
  logd("Starting to read database...");
  while (1)
  {
    // Get the line
    if (fgets(line_buffer, 2048, database_fd) == NULL)
      break;
    logd("Database line read: %s", line_buffer);

    if (db_length == 0)
      db = (user_t **)malloc(++db_length * sizeof(user_t *));
    else
      db = (user_t **)realloc(db, ++db_length * sizeof(user_t *));

    db[db_length - 1] = (user_t *)malloc(sizeof(user_t));

    // Parse
    current_token = strtok_r(line_buffer, COMMAND_DELIMITER, &context);
    if (current_token == NULL)
      break;
    db[db_length - 1]->id = atoi(current_token);

    current_token = strtok_r(NULL, COMMAND_DELIMITER, &context);
    if (current_token == NULL)
      break;
    snprintf(db[db_length - 1]->nickname, strlen(current_token) + 1, "%s", current_token);

    current_token = strtok_r(NULL, COMMAND_DELIMITER, &context);
    if (current_token == NULL)
      break;
    current_token[strlen(current_token) - 1] = '\0'; // Last token has \n

    snprintf(db[db_length - 1]->password, strlen(current_token) + 1, "%s", current_token);

    // Init sem for user
    sem_init(&db[db_length - 1]->messages_write_sem, 0, 1); // binary sem

    logd("Loaded user: #%d %s:%s",
         db[db_length - 1]->id, db[db_length - 1]->nickname, db[db_length - 1]->password);
  }
  fclose(database_fd);
  return 0;
}

/**
 * Add a user to database. This method perform both an adding to the server db file
 * and an adding to global variable db with a realloc
 *
 * @param user
 * @return -1 if error, 0 if success
 */
int add_user_to_database(user_t *user)
{
  sem_wait(&write_db_sem); // Critical section
  // Assign id to user
  user->id = db_length;
  size_t to_write_len = strlen(user->nickname) + strlen(user->password) + 10;
  char *to_write = (char *)malloc(to_write_len);
  snprintf(to_write, to_write_len, "%d;%s;%s\n", db_length, user->nickname, user->password);

  FILE *database_fd = fopen(SERVER_FILES_PATH DATABASE_FILENAME, "a");
  if (database_fd == NULL)
    return -1;
  fprintf(database_fd, to_write, to_write_len);
  fclose(database_fd);
  free(to_write);

  // Add user to memory
  if (db_length == 0)
    db = (user_t **)malloc(++db_length * sizeof(user_t *));
  else
    db = realloc(db, ++db_length * sizeof(user_t *));
  db[db_length - 1] = user;
  // Init sem for user
  sem_init(&db[db_length - 1]->messages_write_sem, 0, 1); // binary sem
  sem_post(&write_db_sem);

  logd("[SERVER] Added user: #%d %s:%s",
       db[db_length - 1]->id, db[db_length - 1]->nickname, db[db_length - 1]->password);
  if (DB_DEBUG)
    print_db();
  return 0;
}

/**
 * Release all resources for users
 */
void free_database()
{
  for (int i = 0; i < db_length; i++)
  {
    sem_close(&db[i]->messages_write_sem);
    free(db[i]);
  }
  free(db);
}

void print_db()
{
  int i;
  logd("======== START DB_DEBUG ========");
  for (i = 0; i < db_length; i++)
    logd("[#%d] %s:%s", db[i]->id, db[i]->nickname, db[i]->password);
  logd("======== END DB_DEBUG ========");
}

void handle_sigint_server(int i)
{
  sem_close(&write_db_sem);
  free_database();
  logs("Messaging app server closed successfully!");
  exit(EXIT_SUCCESS);
}

void register_signals()
{
  struct sigaction act_pipe;
  sigset_t sigset_pipe;
  sigemptyset(&sigset_pipe);
  act_pipe.sa_handler = SIG_IGN;
  act_pipe.sa_mask = sigset_pipe;
  act_pipe.sa_flags = 0;
  sigaction(SIGPIPE, &act_pipe, NULL);

  struct sigaction act_int;
  sigset_t sigset_int;
  sigemptyset(&sigset_int);
  sigaddset(&sigset_int, SIGINT);
  act_int.sa_handler = handle_sigint_server;
  act_int.sa_mask = sigset_int;
  act_int.sa_flags = 0;
  sigaction(SIGINT, &act_int, NULL);

  struct sigaction act_term;
  sigset_t sigset_term;
  sigemptyset(&sigset_term);
  sigaddset(&sigset_term, SIGINT);
  sigaddset(&sigset_term, SIGTERM);
  act_term.sa_handler = handle_sigint_server;
  act_term.sa_mask = sigset_term;
  act_term.sa_flags = 0;
  sigaction(SIGTERM, &act_term, NULL);
}
