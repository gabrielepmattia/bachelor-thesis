/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  client_ui.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 28, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <common.h>
#include <client.h>
#include <ui.h>
#include <log.h>

#include <string.h>
#include <time.h>

char current_window;

char draft_to[MAX_USER_STR_SIZE] = {0};
char draft_subject[STR_BUFFER_SIZE] = {0};
char draft_message[STR_BUFFER_SIZE] = {0};

char response_message[STR_BUFFER_SIZE] = {0};

static const char default_date_format[] = "%a %b %d %Y @ %H:%M:%S";

extern unsigned int logging_token;
extern unsigned int user_id;

/**
 * This function starts the user interface, and it's the main controller that
 * parses commands and changes interfaces
 */
int start_ui()
{
  int ret;
  char input[STR_BUFFER_SIZE];
  server_command_t command_to_send;
  server_response_t response_t;

  init_ui();
  welcome_w();

  while (1)
  {
    ret = get_input(&input); // This is blocking
    if (ret < 0)
      return 0; // Returns -1 if requested cancel

    if (strcmp(input, "exit") == 0)
      handle_sigint_client(0);

    if (current_window == SEND_WINDOW)
    {
      if (strncmp(input, "back", 4) == 0)
      {
        welcome_w();
        bzero(&response_message, strlen(response_message));
        continue;
      }
      if (strncmp(input, "to", 2) == 0)
        snprintf(draft_to, MAX_USER_STR_SIZE, input + 2 + 1);
      if (strncmp(input, "subj", 4) == 0)
        snprintf(draft_subject, STR_BUFFER_SIZE, input + 4 + 1);
      if (strncmp(input, "msg", 3) == 0)
        snprintf(draft_message, STR_BUFFER_SIZE, input + 3 + 1);
      if (strncmp(input, "clean", 5) == 0)
      {
        bzero(&draft_to, MAX_LINE_BUFFER_SIZE);
        bzero(&draft_subject, STR_BUFFER_SIZE);
        bzero(&draft_message, STR_BUFFER_SIZE);
        //bzero(&response_message, STR_BUFFER_SIZE);
      }

      bzero(&response_message, strlen(response_message));

      if (strncmp(input, "send", 4) == 0)
      {

        command_to_send.cmd = SEND_MESSAGE_SERVER_CMD;
        command_to_send.args_n = 5; // user_id, token, recipient, subj, msg
        snprintf(command_to_send.args[0], STR_BUFFER_SIZE, "%d", user_id);
        snprintf(command_to_send.args[1], STR_BUFFER_SIZE, "%d", logging_token);
        snprintf(command_to_send.args[2], STR_BUFFER_SIZE, "%s", draft_to);
        snprintf(command_to_send.args[3], STR_BUFFER_SIZE, "%s", draft_subject);
        snprintf(command_to_send.args[4], STR_BUFFER_SIZE, "%s", draft_message);

        ret = send_command_to_server(&command_to_send, &response_t);
        if (ret < 0)
          snprintf(response_message, STR_BUFFER_SIZE, "Server did not answer, or answer was broken");
        else
          snprintf(response_message, STR_BUFFER_SIZE, response_t.args[0]);

        free_server_response(&response_t);
      }

      send_w();
    }

    if (current_window == SYNC_WINDOW)
    {
      if (strncmp(input, "back", 4) == 0)
      {
        welcome_w();
        bzero(&response_message, strlen(response_message));
        continue;
      }
      if (strncmp(input, "refresh", 7) == 0)
      {
        sync_a();
      }
    }

    if (current_window == INBOX_WINDOW)
    {
      if (strncmp(input, "back", 4) == 0)
      {
        welcome_w();
        bzero(&response_message, strlen(response_message));
        continue;
      }
      if (strncmp(input, "del", 3) == 0)
      {
        command_to_send.cmd = DEL_MESSAGE_SERVER_CMD;
        command_to_send.args_n = 3;
        snprintf(command_to_send.args[0], STR_BUFFER_SIZE, "%d", user_id);
        snprintf(command_to_send.args[1], STR_BUFFER_SIZE, "%d", logging_token);
        snprintf(command_to_send.args[2], STR_BUFFER_SIZE, "%s", input + 4);

        ret = send_command_to_server(&command_to_send, &response_t);
        if (ret < 0)
          snprintf(response_message, STR_BUFFER_SIZE, "Server did not answer, or answer was broken");
        else
          snprintf(response_message, STR_BUFFER_SIZE, response_t.args[0]);
        free_server_response(&response_t);
        sync_a();
        list_w();
      }
    }

    if (strcmp(input, "send") == 0)
    {
      send_w();
      continue;
    }

    if (strcmp(input, "sync") == 0)
    {
      //sync_w();
      sync_a();
      sync_w();
      continue;
    }

    if (strcmp(input, "list") == 0)
    {
      bzero(&response_message, strlen(response_message));
      list_w();
      continue;
    }

    refreshs();
  }

  return 0;
}

void welcome_w()
{
  current_window = MAIN_WINDOW;
  flushs();
  prints("");
  prints(" >> Home");
  prints("");
  prints(" Welcome to the messaging app, you can use the following commands:");
  prints("\tlist - list all messages");
  prints("\tsync - sync the messages from server");
  prints("\tsend - send a message to a user");
  prints("\texit - exit from the application");
  refreshs();
}

void send_w()
{
  current_window = SEND_WINDOW;
  flushs();
  prints("");
  prints(" >> Send a message");
  prints(" To fill message info type:");
  prints("\t- 'to <username>' - to fill recipient username");
  prints("\t- 'subj <subject>' - to fill message subject");
  prints("\t- 'msg <message>' - to fill message content");
  prints("\t- 'send' to send message and 'clean' to reset form");
  prints("");
  prints(" >>> Message composition");
  prints("\tTo: %s", draft_to);
  prints("\tSubject: %s", draft_subject);
  prints("\tMessage: %s", draft_message);
  prints("");
  prints("%s", response_message);
  refreshs();
}

void sync_w()
{
  current_window = SYNC_WINDOW;
  flushs();
  prints("");
  prints(" >> Syncing");
  prints("");
  prints(" When ready type 'back' to return to main menu or 'list' to list messages");
  prints("");
  prints("\t%s", response_message);
  refreshs();
}

void list_w()
{
  current_window = INBOX_WINDOW;
  int n_messages = 0, n_line_elems;
  char line_buffer[MAX_LINE_BUFFER_SIZE];
  char **line_elems;
  const char *date_format = default_date_format;

  time_t timestamp;
  char time_s[32];
  struct tm lt;

  flushs();

  prints("");
  prints(" >> Inbox");
  prints("\t%s", response_message);

  char messages_files_path[STR_BUFFER_SIZE];
  snprintf(messages_files_path, STR_BUFFER_SIZE, "%s%s-%d", CLIENT_FILES_PATH, "messages", user_id);

  FILE *messages_fd = fopen(messages_files_path, "r");
  if (messages_fd == NULL)
    prints("\tYou have no messages yet!");
  else
  {
    while (1)
    {
      if (fgets(line_buffer, MAX_LINE_BUFFER_SIZE, messages_fd) == NULL)
        break;
      n_line_elems = generic_parser(line_buffer, &line_elems); // Parse message string

      // Prepare date
      timestamp = (time_t)atoi(line_elems[0]); // Unix timestamp
      localtime_r(&timestamp, &lt);
      if (strftime(time_s, sizeof(time_s), date_format, &lt) == 0)
        loge("Can't convert timestamp to date");

      prints("#%d [%s][From: %s] %s",
             n_messages,
             time_s,         // Rendered date
             line_elems[2],  // Sender name
             line_elems[3]); // Subject
      line_elems[4][strlen(line_elems[4]) - 1] = '\0';
      prints("\t%s", line_elems[4]);
      printls("-");

      free_matrix(line_elems, n_line_elems);
      n_messages++;
    }
    fclose(messages_fd);
    if (n_messages == 0)
      prints("\tYou have no messages yet!");
  } // if has messages

  refreshs();
}

// Actions
int sync_a()
{
  int ret;
  server_command_t command_to_send;
  server_response_t response_t;

  command_to_send.cmd = SYNC_SERVER_CMD;
  command_to_send.args_n = 2; // user_id, token
  snprintf(command_to_send.args[0], STR_BUFFER_SIZE, "%d", user_id);
  snprintf(command_to_send.args[1], STR_BUFFER_SIZE, "%d", logging_token);

  char buffer[STR_BUFFER_SIZE];
  prepare_string_from_command(&command_to_send, buffer);

  int sock = connect_to_server();
  if (sock < 0)
  {
    loge("Connecting to socker error");
    return -1;
  }

  send_data(sock, buffer, strlen(buffer) + 1);
  ret = receive_data(sock, buffer, STR_BUFFER_SIZE);
  if (sock < 0)
  {
    loge("Error receiveing response from server");
    return -1;
  }

  ret = parse_server_response(buffer, &response_t);
  if (ret < 0)
  {
    snprintf(response_message, STR_BUFFER_SIZE, "Server did not answer, or answer was broken");
    free_server_response(&response_t);
  }
  else
  {
    int n_messages = atoi(response_t.args[0]);

    // Message file path
    char messages_files_path[STR_BUFFER_SIZE];
    snprintf(messages_files_path, STR_BUFFER_SIZE, "%s%s-%d", CLIENT_FILES_PATH, "messages", user_id);

    if (n_messages == 0)
    {
      snprintf(response_message, STR_BUFFER_SIZE, "There's no message for you at the moment!");
      free_server_response(&response_t);
      fclose(fopen(messages_files_path, "w"));
      sync_w();
    }
    else
    {
      free_server_response(&response_t);

      // Start downloading messages
      FILE *messages_fd = fopen(messages_files_path, "w");
      if (messages_fd == NULL)
        loge("Can't save logging token to file");

      for (int i = 0; i < n_messages; i++)
      {
        ret = receive_data(sock, buffer, STR_BUFFER_SIZE); // Other answers are code;timestamp;sender;subj;message

        // Write message to file
        fprintf(messages_fd, "%s\n", buffer);
      }
      ret = close(sock);
      fclose(messages_fd);

      snprintf(response_message, STR_BUFFER_SIZE, "Synced %d messages", n_messages); // first answer has code,1,numberofmessages
    }                                                                                // has messages
  }                                                                                  // Correct response

  return 0;
}
