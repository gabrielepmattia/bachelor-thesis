/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  log.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 26, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <common.h>
#include <time.h>

#include <stdarg.h>

#include <log.h>

#define INFO_LTR "I"
#define ERROR_LTR "E"
#define DEBUG_LTR "D"
#define SUCCESS_LTR "S"
#define FATAL_LTR "FATAL"

#define RED_COLOR_TAG "\033[0;31m"
#define GREEN_COLOR_TAG "\033[0;32m"
#define YELLOW_COLOR_TAG "\033[0;33m"
#define CYAN_COLOR_TAG "\033[0;36m"

#define BOLD_TAG "\e[1m"
#define DIM_TAG "\e[2m"
#define UNDERLINED_TAG "\e[4m"
#define REVERSE_TAG "\e[7m"
#define HIDDEN_TAG "\e[8m"

#define END_TAG "\e[0;0m"
#define END_BOLD "\e[21m"

/**
 *
 * @param format
 * @param ...
 * @return
 */
int logi(char *format, ...)
{
  va_list arg;
  int done;
  char buf[MAX_BUFFER_SIZE];

  va_start(arg, format);
  done = vsnprintf(buf, MAX_BUFFER_SIZE, format, arg);
  va_end(arg);

#ifdef COLOR_LOG
  printf(CYAN_COLOR_TAG DIM_TAG "[" INFO_LTR "]" END_BOLD " %s" END_TAG "\n", buf);
#else
  printf("[" INFO_LTR "] %s\n", buf);
#endif

  return done;
}

/**
 *
 * @param format
 * @param ...
 * @return
 */
int loge(char *format, ...)
{
  va_list arg;
  int done;
  char buf[MAX_BUFFER_SIZE];

  va_start(arg, format);
  done = vsnprintf(buf, MAX_BUFFER_SIZE, format, arg);
  va_end(arg);

#ifdef COLOR_LOG
  printf(RED_COLOR_TAG DIM_TAG "[" ERROR_LTR "]" END_BOLD " %s" END_TAG "\n", buf);
#else
  printf("[" ERROR_LTR "] %s\n", buf);
#endif

  return done;
}

/**
 *
 * @param format
 * @param ...
 * @return
 */
int logd(char *format, ...)
{
  va_list arg;
  int done;
  char buf[MAX_BUFFER_SIZE];

  va_start(arg, format);
  done = vsnprintf(buf, MAX_BUFFER_SIZE, format, arg);
  va_end(arg);

#ifdef COLOR_LOG
  printf(YELLOW_COLOR_TAG DIM_TAG "[" DEBUG_LTR "]" END_BOLD " %s" END_TAG "\n", buf);
#else
  printf("[" DEBUG_LTR "] %s\n", buf);
#endif

  return done;
}

/**
 *
 * @param format
 * @param ...
 * @return
 */
int logs(char *format, ...)
{
  va_list arg;
  int done;
  char buf[MAX_BUFFER_SIZE];

  va_start(arg, format);
  done = vsnprintf(buf, MAX_BUFFER_SIZE, format, arg);
  va_end(arg);

#ifdef COLOR_LOG
  printf(GREEN_COLOR_TAG DIM_TAG "[" SUCCESS_LTR "]" END_BOLD " %s" END_TAG "\n", buf);
#else
  printf("[" SUCCESS_LTR "] %s\n", buf);
#endif

  return done;
}

// Side-effect Implementations

/**
 * Log a fatal error. This cause the application to terminate
 * @param format
 * @param ...
 */
void logfe(char *format, ...)
{
  va_list arg;
  char buf[MAX_BUFFER_SIZE];

  va_start(arg, format);
  vsnprintf(buf, MAX_BUFFER_SIZE, format, arg);
  va_end(arg);

#ifdef COLOR_LOG
  printf(RED_COLOR_TAG DIM_TAG "[" FATAL_LTR "]" END_BOLD " %s" END_TAG "\n", buf);
#else
  printf("[" FATAL_LTR "] %s\n", buf);
#endif

  exit(EXIT_FAILURE);
}
