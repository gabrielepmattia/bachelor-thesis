/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  client.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 28, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <common.h>

#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>  // htons()
#include <netinet/in.h> // struct sockaddr_in
#include <sys/socket.h>
#include <sys/stat.h>
#include <signal.h>

#include <ctype.h>
#include <getopt.h>

#include <client.h>
#include <ui.h>
#include <log.h>

unsigned int logging_token;
unsigned int user_id;

int socket_fd; // Main socket

int main(int argc, char **argv)
{
  register_signals();

  int ret;
  ttyclear();
  printf("This is a Messaging Application.\n");
  printf("Written by Gabriele Proietti Mattia - Facoltà di Ingegneria Informatica\n");
  printf("Operating Systems Project - Teacher: Francesco Quaglia\n\n\n");

  printf("To exit hit Ctrl+C any moment or type \"exit\" when in interactive mode\n\n");

  // Create client files dir
  while ((ret = mkdir(CLIENT_FILES_PATH, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0))
  {
    if (errno == EEXIST)
      break;
    logfe("Can't create server files " CLIENT_FILES_PATH " dir");
  }

  // Create the possible actions
  int register_option_set = 0;
  struct option register_opt = {"register", no_argument, NULL, 'r'}; // Option #0
  int login_option_set = 0;
  struct option login_opt = {"login", no_argument, NULL, 'l'}; // Option #1
  struct option long_options[] = {register_opt, login_opt};

  // Get the action requested
  while (1)
  {
    int option_index = 0, current_option = 0;
    current_option = getopt_long(argc, argv, "rl", long_options, &option_index);
    if (current_option == -1)
      break;
    switch (current_option)
    {
    case 'r':
      if (login_option_set)
      {
        printf("Only one --instruction at time");
        abort();
      }
      register_option_set = 1;
      printf("Starting registering procedure...\n");
      ret = register_procedure();
      break;
    case 'l':
      if (register_option_set)
      {
        printf("Only one --instruction at time");
        abort();
      }
      login_option_set = 1;
      printf("Starting login procedure...\n");
      ret = login_procedure();
      break;
    default:
      abort();
    }
  }
  if (ret < 0)
    printf("Something went wrong, please retry!\n\n");

  exit(EXIT_SUCCESS);
}

/* Procedures */
int register_procedure()
{
  int ret;
  char nickname[MAX_USER_STR_SIZE];
  char password[MAX_USER_STR_SIZE];
  char password_check[MAX_USER_STR_SIZE];
  server_response_t response_t;
  printf("Please fill the following information to register.\n");
  while (1)
  {
    bzero(&nickname, MAX_USER_STR_SIZE);
    bzero(&password, MAX_USER_STR_SIZE);
    bzero(&password_check, MAX_USER_STR_SIZE);

    printf("=> Username [31 chars max]: ");
    fgets(nickname, MAX_USER_STR_SIZE, stdin);
    int nickname_length = strlen(nickname);
    nickname[nickname_length - 1] = '\0';

    set_input_mode(~ECHO, 1);
    printf("=> Choose a Password [31 chars max]: ");
    fgets(password, MAX_USER_STR_SIZE, stdin);
    int password_length = strlen(password);
    password[password_length - 1] = '\0';
    printf("\n");

    printf("=> Retype Password: ");
    fgets(password_check, MAX_USER_STR_SIZE, stdin);
    password_check[strlen(password_check) - 1] = '\0';
    printf("\n");
    reset_input_mode();

    logd("User: %s, Password: %s", nickname, password);
    if (strcmp(password, password_check) != 0)
    {
      loge("Password are different! Please retry!");
      continue;
    }

    server_command_t command_to_send;
    command_to_send.cmd = REGISTER_SERVER_CMD;
    command_to_send.args_n = 2;
    strncpy(command_to_send.args[0], nickname, strlen(nickname) + 1);
    strncpy(command_to_send.args[1], password, strlen(password) + 1);

    ret = send_command_to_server(&command_to_send, &response_t);
    if (ret < 0)
    {
      loge("Server did not answer, or answer was broken");
      continue;
    }

    if (response_t.result_code < 0)
    {
      loge("There was an error during the register procedure!");
      logi("Server Message: %s \n", response_t.args[0]);
    }
    else
    {
      printf("You has been registered successfully!\n");
      break;
    }
  }
  free_server_response(&response_t);
  return 0;
}

int login_procedure()
{
  int ret;
  char nickname[MAX_USER_STR_SIZE], password[MAX_USER_STR_SIZE];
  server_response_t response_t;
  while (1)
  {
    printf("Please fill login information.\n");

    printf("=> Username: ");
    fgets(nickname, MAX_USER_STR_SIZE, stdin);
    int nickname_length = strlen(nickname);
    nickname[nickname_length - 1] = '\0';

    set_input_mode(~ECHO, 1);
    printf("=> Password: ");
    fgets(password, MAX_USER_STR_SIZE, stdin);
    int password_length = strlen(password);
    password[password_length - 1] = '\0';
    printf("\n");
    reset_input_mode();

    server_command_t command_to_send;
    command_to_send.cmd = LOGIN_SERVER_CMD;
    command_to_send.args_n = 2;
    strncpy(command_to_send.args[0], nickname, strlen(nickname) + 1);
    strncpy(command_to_send.args[1], password, strlen(password) + 1);

    ret = send_command_to_server(&command_to_send, &response_t);
    if (ret < 0)
    {
      loge("Server did not answer, or answer was broken");
      continue;
    }

    if (response_t.result_code < 0)
    {
      loge("There was an error during the login procedure!");
      logi("Server Message: %s \n", response_t.args[0]);
      free_server_response(&response_t);
    }
    else
    {
      printf("You has been logged successfully, your id is %s and your logging token is #%s\n",
             response_t.args[0], response_t.args[1]);

      user_id = atoi(response_t.args[0]);
      logging_token = atoi(response_t.args[1]);

      // Token path
      char token_path[STR_BUFFER_SIZE];
      snprintf(token_path, STR_BUFFER_SIZE, "%s%s-%d", CLIENT_FILES_PATH, TOKEN_FILENAME, user_id);

      FILE *token_fd = fopen(token_path, "w");
      if (token_fd == NULL)
        loge("Can't save logging token to file");
      fprintf(token_fd, response_t.args[1], strlen(response_t.args[1]));
      fclose(token_fd);

      free_server_response(&response_t);

      ret = start_ui(); // Returns when asked canceling
    }
  }
  return 0;
}

/**
 * Connect so server by creating a socket
 * @return socket connected to server
 */
int connect_to_server()
{
  int ret;

  struct sockaddr_in server_address = {0};
  server_address.sin_addr.s_addr = inet_addr(SERVER_ADDRESS);
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(SERVER_PORT);

  socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd < 0)
  {
    loge("Error creating the socket to server");
    return -1;
  }

  ret = connect(socket_fd, (struct sockaddr *)&server_address, sizeof(struct sockaddr_in));
  if (ret < 0)
  {
    loge("Can't connect to server");
    return -1;
  }

  return socket_fd;
}

/**
 * Receive data from already opened and connected socket. \0 signs the end of data
 * @param  socket_fd socket descriptior
 * @return           received bytes
 */
int receive_data(int socket_fd, char *buf, size_t buf_len)
{
  int ret, recv_bytes = 0;

  while (1)
  {
    ret = recv(socket_fd, buf + recv_bytes, 1, 0);
    if (ret < 0)
    {
      if (errno == EINTR)
        continue;
      loge("Cannot read from socket");
    }
    if (ret == 0)
    {
      logfe("Peer improperly closed the connection");
      break;
    }

    recv_bytes += ret;
    if (buf[recv_bytes - 1] == '\0')
      break;
  }

  return recv_bytes;
}

/**
 * Send data from already opened and connected socket
 * @param  socket_fd socket descriptor
 * @param  buf       pointer to preallocated buffer
 * @param  buf_len   buffer length
 * @return           sent bytes
 */
int send_data(int socket_fd, char *buf, size_t buf_len)
{
  int ret;
  while ((ret = send(socket_fd, buf, buf_len, 0)) < 0)
  {
    if (errno == EINTR)
      continue;
    if (errno == EPIPE)
      loge("Broken pipe");
    loge("Can't send data to server");
    break;
  }
  return ret;
}

/**
 * Send a command to server passing a server_command_t*
 * @param command command to send
 * @return result code
 */
int send_command_to_server(server_command_t *command_t, server_response_t *response_t)
{
  char command_s[STR_BUFFER_SIZE];
  prepare_string_from_command(command_t, command_s);
  return send_data_to_server(command_s, strlen(command_s) + 1, response_t);
}

/**
 * Send a string to the server and receive a response
 * @param msg pointer to buffer
 * @param msg_len length buffer
 * @return result code
 */
int send_data_to_server(char *msg, int msg_len, server_response_t *response)
{
  int ret, recv_bytes = 0;

  connect_to_server();

  ret = send_data(socket_fd, msg, msg_len);
  if (ret < 0)
    return ret;

  // Wait and receive the response
  char buf[STR_BUFFER_SIZE * 5];
  int buf_len = sizeof(buf);

  recv_bytes = receive_data(socket_fd, buf, buf_len);

  buf[recv_bytes] = '\0';
  if (parse_server_response(buf, response) < 0)
  {
    loge("Server response malformed");
    return -1;
  }

  ret = close(socket_fd);
  if (ret < 0)
    loge("Can't close the server socket");
  return 0;
}

/**
 * SIGINT handling
 */
void handle_sigint_client(int i)
{
  ttyclear();
  close_ui();
  close(socket_fd);
  logs("Messaging app closed successfully!");
  exit(EXIT_SUCCESS);
}

/**
 * Register signals here
 */
void register_signals()
{
  struct sigaction act_pipe;
  sigset_t sigset_pipe;
  sigemptyset(&sigset_pipe);
  act_pipe.sa_handler = SIG_IGN;
  act_pipe.sa_mask = sigset_pipe;
  act_pipe.sa_flags = 0;
  sigaction(SIGPIPE, &act_pipe, NULL);

  struct sigaction act_int;
  sigset_t sigset_int;
  sigemptyset(&sigset_int);
  sigaddset(&sigset_int, SIGINT);
  act_int.sa_handler = handle_sigint_client;
  act_int.sa_mask = sigset_int;
  act_int.sa_flags = 0;
  sigaction(SIGINT, &act_int, NULL);

  struct sigaction act_term;
  sigset_t sigset_term;
  sigemptyset(&sigset_term);
  sigaddset(&sigset_term, SIGINT);
  sigaddset(&sigset_term, SIGTERM);
  act_term.sa_handler = handle_sigint_client;
  act_term.sa_mask = sigset_term;
  act_term.sa_flags = 0;
  sigaction(SIGTERM, &act_term, NULL);
}
