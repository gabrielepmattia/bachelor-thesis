/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  testui.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 26, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <common.h>
#include <ui.h>

#include <sys/ioctl.h>
#include <unistd.h>

void welcome()
{
  flushs();
  prints("");
  prints(" >> Hello World! UI is live!");
  prints("This is a sample text to show how the ui works");
  prints("");
  prints("Some text, some text some text some text some text some text some text");
  prints("Some text, some text some text some text some text some text some text");
  prints("Some text, some text some text some text some text some text some text");
  prints("Some text, some text some text some text some text some text some text");
  prints("Some text, some text some text some text some text some text some text");
  prints("Some text, some text some text some text some text some text some text");
  prints("Some text, some text some text some text some text some text some text");
  prints("Some text, some text some text some text some text some text some text");
  refreshs();
}

int main(int argc, char **argv)
{
  char input[STR_BUFFER_SIZE];
  init_ui();
  welcome();

  while (1)
  {
    get_input(&input);
    prints(input);
    refreshs();
  }

  return 0;
}
