/**
 * Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
 * 
 * This file is part of Bachelor Thesis.
 * 
 * Bachelor Thesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Bachelor Thesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// =============================================
//  test.c
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 27, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#include <stdio.h>
#include <stdlib.h>
#include <common.h>
#include <log.h>
#include <netinet/in.h>
#include <errno.h>
#include <strings.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <unistd.h>

void test_parse_server_response()
{
  server_response_t response_tt;
  char response_s[] = "0;2;first argument;second argument";
  parse_server_response(response_s, &response_tt);

  logd("[parse_server_response] response_tt->result_code: %d", response_tt.result_code);
  logd("[parse_server_response] response_tt->args_n: %d", response_tt.args_n);
  logd("[parse_server_response] response_tt->args[0]: %s", response_tt.args[0]);
  logd("[parse_server_response] response_tt->args[1]: %s", response_tt.args[1]);
}

void test_prepare_string_from_response()
{
  server_response_t *response_tt = (server_response_t *)malloc(sizeof(server_response_t));
  char response_s[STR_BUFFER_SIZE];

  response_tt->result_code = 0;
  response_tt->args_n = 1;
  response_tt->args = (char **)malloc(sizeof(char *));
  response_tt->args[0] = (char *)malloc(10 * sizeof(char));
  snprintf(response_tt->args[0], 10, "Hello");

  prepare_string_from_response(response_tt, response_s);

  logd("[parse_server_response] result: %s", response_s);
  free_server_response(response_tt);
}

void test_generic_parse()
{
  char line[] = "first;second;third;4;5";
  char **args;
  int n = generic_parser(line, &args);
  logd("[test_generic_parse] arg0 :: %s", args[0]);
  logd("[test_generic_parse] arg1 :: %s", args[1]);
  logd("[test_generic_parse] arg2 :: %s", args[2]);
  logd("[test_generic_parse] arg3 :: %s", args[3]);
  logd("[test_generic_parse] arg4 :: %s", args[4]);
  free_matrix(args, n);
}

int main(int argc, char **argv)
{
  logi("Info text example %d %d read %s", 4, 6, "hello");
  loge("Error text example %d %d %s", 4, 6, "hello");
  logd("Debug info example %d %d %s", 4, 6, "hello");
  logs("Success info example %d %d %s", 4, 6, "hello");

  test_parse_server_response();
  test_prepare_string_from_response();
  test_generic_parse();

  exit(EXIT_SUCCESS);
}
