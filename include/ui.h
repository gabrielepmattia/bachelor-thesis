// Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
//
// This file is part of Bachelor Thesis.
//
// Bachelor Thesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bachelor Thesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
//

// =============================================
//  ui.h
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 27, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#ifndef UI_H
#define UI_H

#define MAX_LINE_BUFFER_SIZE 2048

#include <sys/ioctl.h>
#include <unistd.h>
#include <termios.h>

#define KEY_UP "\027[A"    // UP    is 27 91 65
#define KEY_DOWN "\027[B"  // DOWN  is 27 91 66
#define KEY_LEFT "\027[D"  // LEFT  is 27 91 67
#define KEY_RIGHT "\027[C" // RIGHT is 27 91 68

#define COMMAND_LINE_CHR ">_ "
#define COMMAND_LINE_CHR_N 3

#define move_cursor_up() printf("\e[A");
#define move_cursor_down() printf("\e[B");
#define move_cursor_right() printf("\e[C");
#define move_cursor_left() printf("\e[D");

int init_ui();
int prints(char *format, ...);
void printls(char *c);
int refreshs();
int flushs();
int get_input();

int flushs_screen();
void close_ui();

// Threads
void *ui_core();

// Termios
void reset_input_mode();
void set_input_mode();

#endif /* UI_H */
