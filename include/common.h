// Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
//
// This file is part of Bachelor Thesis.
//
// Bachelor Thesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bachelor Thesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
//

// =============================================
//  common.h
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 27, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>

// Configuration parameters
#define SERVER_PORT 3030
#define MAX_SERVER_CONNECTIONS 20
#define SERVER_ADDRESS "127.0.0.1"
// Server functions
#define REGISTER_SERVER_CMD 'r'
#define SEND_MESSAGE_SERVER_CMD 's'
#define SYNC_SERVER_CMD 'S'
#define LOGIN_SERVER_CMD 'l'
#define DEL_MESSAGE_SERVER_CMD 'd'
// Others
#define COMMAND_DELIMITER ";"
#define COMMAND_MAX_ARGS 10
#define STR_BUFFER_SIZE 2048
#define BUFFER_SIZE 4096
#define MAX_USER_STR_SIZE 32

#define ttyclear() printf("\033c")

#define DEBUG 1
#define DB_DEBUG 1

typedef struct server_response_s
{
    int result_code;
    int args_n;
    char **args;
} server_response_t;

typedef struct server_command_s
{
    char cmd;
    int args_n; // Number of arguments
    char args[COMMAND_MAX_ARGS][STR_BUFFER_SIZE];
} server_command_t;

int parse_server_response(char *response_s_in, server_response_t *response_t_out);
int parse_server_command(char *command_s_in, server_command_t *command_t_out);
int prepare_string_from_command(server_command_t *command_t_in, char *command_s_out);
int prepare_string_from_response(server_response_t *response_t_in, char *response_s_out);
int generic_parser(char *s_in, char ***out);

void free_server_response(server_response_t *response_t_in);
void free_matrix(char **m, int n);

#endif /* COMMON_H */
