// Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
//
// This file is part of Bachelor Thesis.
//
// Bachelor Thesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bachelor Thesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
//

// =============================================
//  client.h
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 28, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#ifndef CLIENT_H
#define CLIENT_H

#include <common.h>

#define TOKEN_FILENAME "ltoken"
#define CLIENT_FILES_PATH "client_files/"

#define MAIN_WINDOW 0
#define SEND_WINDOW 1
#define SYNC_WINDOW 3
#define INBOX_WINDOW 4

// GUI
int register_procedure();
int login_procedure();

int connect_to_server();
int close_socket(int socket_fd);
int send_data_to_server(char *msg, int msg_len, server_response_t *response_t);
int send_command_to_server(server_command_t *command, server_response_t *response_t);
int send_data(int socket_fd, char *buf, size_t buf_len);
int receive_data(int socket_fd, char *buf, size_t buf_len);

int start_ui();
void welcome_w();
void send_w();
void sync_w();
void list_w();
int sync_a();

void handle_sigint_client(int i);
void register_signals();
#endif /* CLIENT_H */
