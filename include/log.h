// Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
//
// This file is part of Bachelor Thesis.
//
// Bachelor Thesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bachelor Thesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
//

// =============================================
//  log.h
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 26, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#ifndef LOG_H
#define LOG_H

#include <common.h>

#define MAX_BUFFER_SIZE 2048

int logi(char *format, ...);
int loge(char *format, ...);
int logd(char *format, ...);
int logs(char *format, ...);

void logfe(char *format, ...);

#endif /* UTILS_H */
