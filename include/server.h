// Copyright (C) 2018 Gabriele Proietti Mattia <gabry.gabry@hotmail.it>
//
// This file is part of Bachelor Thesis.
//
// Bachelor Thesis is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bachelor Thesis is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bachelor Thesis.  If not, see <http://www.gnu.org/licenses/>.
//

// =============================================
//  server.h
// =============================================

/**
 * == Servizio di Messagistica ==
 * Progetto di Sistemi Operativi a.a. 2016/2017
 * Facolta' di Ingegneria Informatica
 * Universita' La Sapienza di Roma
 * Docente: Francesco Quaglia
 *
 * @Autor:       Gabriele Proietti Mattia <gabry3795>
 * @Created on:  Jan 27, 2017
 * @Email:       gabry.gabry@hotmail.it
 */

#ifndef SERVER_H
#define SERVER_H

#include <common.h>
#include <semaphore.h>

#define SERVER_ERROR_CODE -1
#define SERVER_OK_CODE 0

#define DATABASE_FILENAME "db"
#define SERVER_FILES_PATH "server_files/"

typedef struct connection_handler_args_s
{
    int socket_fd;
    struct sockaddr_in *address;
} connection_handler_args_t;

typedef struct user_s
{
    unsigned int id;
    char nickname[MAX_USER_STR_SIZE];
    char password[MAX_USER_STR_SIZE];
    unsigned long int logged_time;
    unsigned int logged_token;
    sem_t messages_write_sem;
} user_t;

void *client_connection_handler(void *args);
int answer_data_to_client(int socket_fd, char *answer, int answer_length);
int answer_to_client(int socket_fd, server_response_t *response);
int load_database();
int add_user_to_database(user_t *user);
void print_db();

int make_response_single_arg(int socket_fd, int code, char *message);

void handle_sigint_server(int i);
void register_signals();

#endif /* SERVER_H */
