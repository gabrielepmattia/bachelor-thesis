#!/bin/sh
pdflatex -shell-escape proj-thesis.tex
biber proj-thesis
# Yes, compile other two times to get a stable version
pdflatex -shell-escape proj-thesis.tex
pdflatex -shell-escape proj-thesis.tex
